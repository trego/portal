<?php

namespace App\Console;

use App\Console\Commands\Route\CacheRoute;
use App\Console\Commands\Route\DeleteRoute;
use App\Console\Commands\Route\ListRoute;
use App\Console\Commands\Route\UpdateRoute;
use App\Console\Commands\Service\CreateService;
use App\Console\Commands\Service\DeleteService;
use App\Console\Commands\Service\ListService;
use App\Console\Commands\Service\UpdateService;
use App\Console\Commands\Route\AddRoute;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ListService::class,
        CreateService::class,
        UpdateService::class,
        DeleteService::class,

        ListRoute::class,
        AddRoute::class,
        UpdateRoute::class,
        DeleteRoute::class,
        CacheRoute::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
