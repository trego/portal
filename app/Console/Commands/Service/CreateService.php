<?php

namespace App\Console\Commands\Service;

use App\Services\RoutingService;
use Illuminate\Console\Command;

class CreateService extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service:create';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'Create a service';

    /**
     * Handles the command.
     *
     * @param RoutingService $service
     * @throws \App\Exceptions\DuplicateServiceException
     */
    public function handle(RoutingService $service)
    {
        $slug = $this->ask('Slug');
        $name = $this->ask('Name');
        $url = $this->ask('URL');

        $service->addService([
            'slug' => $slug,
            'name' => $name,
            'url' => $url,
        ]);

        $this->info('Service ' . $name . ' has been created');
    }
}