<?php

namespace App\Console\Commands\Service;

use App\Exceptions\ServiceNotFoundException;
use App\Services\RoutingService;
use Illuminate\Console\Command;

class DeleteService extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service:delete {slug}';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'Delete a service';

    /**
     * Handles the command.
     *
     * @param RoutingService $service
     * @throws ServiceNotFoundException
     */
    public function handle(RoutingService $service)
    {
        $slug = $this->argument('slug');

        $microservice = $service->findServiceBySlug($slug);

        if (is_null($microservice)) {
            throw new ServiceNotFoundException;
        }

        $service->deleteService($microservice->id);

        $this->info('Service ' . $slug . ' has been deleted');
    }
}