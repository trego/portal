<?php

namespace App\Console\Commands\Service;

use Illuminate\Console\Command;
use App\Services\RoutingService;

class ListService extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service:list';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'List available services';


    public function handle(RoutingService $service)
    {
        $headers = ['Service', 'Name', 'URL'];
        $services = $service->getServices()
            ->map(function ($item) {
                return [
                    'slug' => $item->slug,
                    'name' => $item->name,
                    'url' => $item->url,
                ];
            })
            ->all();

        $this->table($headers, $services);
    }
}
