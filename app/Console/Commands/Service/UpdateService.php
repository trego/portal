<?php

namespace App\Console\Commands\Service;

use App\Exceptions\ServiceNotFoundException;
use App\Services\RoutingService;
use Illuminate\Console\Command;

class UpdateService extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service:update {slug}';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'Update a service';

    /**
     * Handles the command.
     *
     * @param RoutingService $service
     * @throws ServiceNotFoundException
     * @throws \App\Exceptions\DuplicateServiceException
     * @throws \App\Exceptions\FailedServiceUpdateException
     */
    public function handle(RoutingService $service)
    {
        $slug = $this->argument('slug');
        $name = $this->ask('Name');
        $url = $this->ask('URL');

        $microservice = $service->findServiceBySlug($slug);

        if (is_null($microservice)) {
            throw new ServiceNotFoundException;
        }

        $service->updateService($microservice->id, [
            'slug' => $microservice->slug,
            'name' => $name,
            'url' => $url,
        ]);

        $this->info('Service ' . $name . ' has been updated');
    }
}