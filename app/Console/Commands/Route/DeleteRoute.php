<?php

namespace App\Console\Commands\Route;

use App\Exceptions\FailedRouteRemovalException;
use App\Exceptions\RouteNotFoundException;
use App\Exceptions\ServiceNotFoundException;
use App\Services\RoutingService;
use Illuminate\Console\Command;

class DeleteRoute extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service-route:delete {service} {route}';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'Delete a service\'s route';

    public function handle(RoutingService $service)
    {
        $service_slug = $this->argument('service');
        $route_slug = $this->argument('route');

        try {
            $target_service = $service->findServiceBySlug($service_slug);
        } catch (ServiceNotFoundException $e) {
            $this->error($e->getMessage());
            die();
        }

        try {
            $target_route = $service->findRouteBySlug($route_slug);
        } catch (RouteNotFoundException $e) {
            $this->error($e->getMessage());
            die();
        }

        try {
            $service->removeRoute($target_service->id, $target_route->id);

            $this->info('Route ' . $route_slug . ' has been removed');
        } catch (FailedRouteRemovalException $e) {
            $this->error($e->getMessage());
            die();
        }
    }
}