<?php

namespace App\Console\Commands\Route;

use App\Exceptions\DuplicateRouteException;
use App\Exceptions\FailedRouteCreationException;
use App\Exceptions\ServiceNotFoundException;
use App\Services\RoutingService;
use Illuminate\Console\Command;

class AddRoute extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service-route:create {slug}';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'Add route to a service';

    /**
     * @var array
     */
    protected $valid_methods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

    /**
     * Handles the command.
     *
     * @param RoutingService $service
     * @throws DuplicateRouteException
     * @throws ServiceNotFoundException
     */
    public function handle(RoutingService $service)
    {
        $service_slug = $this->argument('slug');

        try {
            $target_service = $service->findServiceBySlug($service_slug);
        } catch (ServiceNotFoundException $e) {
            $this->error($e->getMessage());
            die();
        }

        $route_slug = $this->ask('Slug');

        if (! $service->isRouteSlugUnique($route_slug, $service_slug)) {
            $this->error('Duplicate route name');
            die();
        }

        do {
            $method = $this->anticipate('Route method', $this->valid_methods);
        } while (! in_array($method, $this->valid_methods));

        do {
            $protected = $this->anticipate('Protected', ['Yes', 'No']);
        } while (! in_array($protected, ['Yes', 'No']));

        $namespace = $this->ask('Namespace');
        $path = $this->ask('Local Path');
        $target = $this->ask('Destination Path');
        $description = $this->ask('Description');

        try {
            $service->addRoute($target_service->id, [
                'slug' => $route_slug,
                'method' => $method,
                'path' => $path,
                'description' => $description,
                'namespace' => $namespace,
                'target' => $target,
                'protected' => $protected === 'Yes' ? true : false,
            ]);

            $this->info('Route ' . $route_slug . ' has been created');
        } catch (FailedRouteCreationException $e) {
            $this->error($e->getMessage());
            die();
        }
    }
}