<?php

namespace App\Console\Commands\Route;

use App\Exceptions\RouteCachingDisabledException;
use App\Services\RoutingService;
use Illuminate\Console\Command;

class CacheRoute extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service-route:cache';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'Cache the routes for faster routing';

    /**
     * Handles the command.
     *
     * @param RoutingService $service
     */
    public function handle(RoutingService $service)
    {
        try {
            $service->cacheRoutes();

            $this->info('Routes have been cached');
        } catch (RouteCachingDisabledException $e) {
            $this->error($e->getMessage());
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}