<?php

namespace App\Console\Commands\Route;

use App\Exceptions\FailedRouteUpdateException;
use App\Exceptions\RouteNotFoundException;
use App\Services\RoutingService;
use Illuminate\Console\Command;

class UpdateRoute extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service-route:update {service} {route}';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'Update a service\'s route';

    /**
     * @var array
     */
    protected $valid_methods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

    public function handle(RoutingService $service)
    {
        $service_slug = $this->argument('service');
        $route_slug = $this->argument('route');

        try {
            $target_service = $service->findServiceBySlug($service_slug);
        } catch (ServiceNotFoundException $e) {
            $this->error($e->getMessage());
            die();
        }

        try {
            $target_route = $service->findRouteBySlug($route_slug);
        } catch (RouteNotFoundException $e) {
            $this->error($e->getMessage());
            die();
        }

        do {
            $method = $this->anticipate('Route method', $this->valid_methods);
        } while (! in_array($method, $this->valid_methods));

        do {
            $protected = $this->anticipate('Protected', ['Yes', 'No']);
        } while (! in_array($protected, ['Yes', 'No']));

        $namespace = $this->ask('Namespace');
        $path = $this->ask('Local Path');
        $target = $this->ask('Destination Path');
        $description = $this->ask('Description');

        try {
            $service->updateRoute($target_service->id, $target_route->id, [
                'slug' => $target_route->slug,
                'method' => $method,
                'path' => $path,
                'description' => $description,
                'namespace' => $namespace,
                'target' => $target,
                'protected' => $protected === 'Yes' ? true : false,
            ]);

            $this->info('Route ' . $route_slug . ' has been updated');
        } catch (FailedRouteUpdateException $e) {
            $this->error($e->getMessage());
            die();
        }
    }
}
