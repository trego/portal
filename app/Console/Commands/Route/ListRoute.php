<?php

namespace App\Console\Commands\Route;

use App\Services\RoutingService;
use Illuminate\Console\Command;

class ListRoute extends Command
{
    /**
     * Command signature.
     *
     * @var string
     */
    protected $signature = 'service-route:list {service}';

    /**
     * Description.
     *
     * @var string
     */
    protected $description = 'List routes of a service';

    public function handle(RoutingService $service)
    {
        $service_slug = $this->argument('service');

        try {
            $target_service = $service->findServiceBySlug($service_slug);
        } catch (ServiceNotFoundException $e) {
            $this->error($e->getMessage());
            die();
        }

        $headers = [
            'Service',
            'Protected',
            'Route',
            'Method',
            'Namespace',
            'Public Path',
            'Destination Path',
            'Description'
        ];

        $routes = $target_service->routes
            ->map(function ($item) {
                return [
                   'service' => $item->service->slug,
                   'protected' => $item->protected ? 'Yes' : 'No',
                   'slug' => $item->slug,
                   'method' => $item->method,
                   'namespace' => $item->namespace,
                   'path' => $item->public_url,
                   'target' => $item->target_url,
                   'description' => $item->description,
                ];
            })
            ->sortBy('namespace')
            ->all();

        $this->table($headers, $routes);
    }
}
