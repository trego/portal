<?php

namespace App\Providers;

use App\Services\HttpService;
use App\Services\RoutingService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Trego\Toolkit\Toolkit;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Boots up.
     */
    public function boot()
    {
        $this->app->singleton(HttpService::class, function () {
            $client = new Client;

            return new HttpService($client);
        });

        $this->app->singleton(RoutingService::class, function () {
            return new RoutingService;
        });

        $this->app->singleton(Toolkit::class, function () {
            return new Toolkit([
                'aws_access_key' => env('ACCESS_KEY_AWS'),
                'aws_secret_key' => env('SECRET_KEY_AWS'),
            ]);
        });

        app(RoutingService::class)->registerRoutes(app());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
