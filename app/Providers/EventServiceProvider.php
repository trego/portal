<?php

namespace App\Providers;

use App\Events\RoutesAltered;
use App\Listeners\RebuildRoutesCache;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RoutesAltered::class => [
            RebuildRoutesCache::class,
        ],
    ];
}
