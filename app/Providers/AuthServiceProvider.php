<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Services\HttpService;
use Trego\Toolkit\Toolkit;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $auth_service = new \App\Services\AuthService(app(HttpService::class), app(Toolkit::class));

        $this->app['auth']->viaRequest('api', function ($request) use ($auth_service) {
            return $auth_service->check($request);
        });
    }
}
