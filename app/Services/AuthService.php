<?php

namespace App\Services;

use App\Exceptions\NoAuthorizationHeaderException;
use App\Services\HttpService;
use App\User;
use Illuminate\Http\Request;
use Trego\Toolkit\Toolkit;

class AuthService
{
    /**
     * @var HttpService
     */
    protected $client;

    /**
     * @var Toolkit
     */
    protected $toolkit;
    
    public function __construct(HttpService $http_service, Toolkit $toolkit)
    {
        $this->client = $http_service;
        $this->toolkit = $toolkit;
    }

    /**
     * Validates request and returns the user.
     *
     * @param Request $request
     * @return void
     */
    public function check(Request $request)
    {
        $header = $request->header('Authorization');

        if (is_null($header)) {
            return null;
        }

        $response = $this->client->get(
            $this->toolkit->getConfig('AUTH_SERVICE_URL') . '/auth/whoami',
            [],
            [
                'User-Agent' => $request->header('user-agent'),
                'Authorization' => $request->header('authorization'),
            ]
        );

        if ($response['status'] !== 200) {
            return null;
        }

        $user = new User;
        $user->id = $response['response']->data->id;
        $user->name = $response['response']->data->name;
        $user->email = $response['response']->data->email;
        $user->role = $response['response']->data->role;

        return $user;
    }
}
