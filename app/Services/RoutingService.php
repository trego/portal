<?php

namespace App\Services;

use App\Events\RoutesAltered;
use App\Exceptions\DuplicateRouteException;
use App\Exceptions\DuplicateServiceException;
use App\Exceptions\FailedRouteCreationException;
use App\Exceptions\FailedRouteRemovalException;
use App\Exceptions\FailedRouteUpdateException;
use App\Exceptions\FailedServiceUpdateException;
use App\Exceptions\InvalidRouteMethodException;
use App\Exceptions\RouteCachingDisabledException;
use App\Exceptions\RouteNotFoundException;
use App\Exceptions\ServiceNotFoundException;
use App\Repositories\ServiceRepository;
use App\Route;
use App\Service;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Laravel\Lumen\Application;

class RoutingService
{
    /**
     * @var ServiceRepository
     */
    protected $repo;

    /**
     * @var array
     */
    private $allowed_methods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

    public function __construct()
    {
        $this->repo = new ServiceRepository;
    }

    /**
     * Returns all services.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getServices()
    {
        return $this->repo->get();
    }

    /**
     * Checks whether service's slug is unique.
     *
     * @param $slug
     * @param $except_slug
     * @return bool
     */
    public function isServiceSlugUnique($slug, $except_slug = false)
    {
        $count = Service::where('slug', $slug)->count();

        if ($except_slug) {
            return $count <= 1;
        }

        return $count === 0;
    }

    /**
     * Checks whether route's slug is unique.
     *
     * @param $slug
     * @param null $service_slug
     * @param bool $except_slug
     * @return int
     * @throws ServiceNotFoundException
     */
    public function isRouteSlugUnique($slug, $service_slug = null, $except_slug = false)
    {
        $count = $this->findServiceBySlug($service_slug)
            ->routes()
            ->where(['slug' => $slug])
            ->count();

        if ($except_slug) {
            return $count <= 1;
        }

        return $count === 0;
    }

    /**
     * Adds a service.
     *
     * @param array $data
     * @return mixed
     * @throws DuplicateServiceException
     */
    public function addService(array $data)
    {
        if (! $this->isServiceSlugUnique($data['slug'])) {
            throw new DuplicateServiceException;
        }

        return Service::create([
            'name' => $data['name'],
            'slug' => $data['slug'],
            'url' => $data['url'],
        ]);
    }

    /**
     * Returns service by ID.
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findService($id)
    {
        return $this->repo->find($id);
    }

    /**
     * Returns service by slug
     *
     * @param $slug
     * @return mixed
     * @throws ServiceNotFoundException
     */
    public function findServiceBySlug($slug)
    {
        $service = Service::where('slug', $slug)->first();

        if (is_null($service)) {
            throw new ServiceNotFoundException;
        }

        return $service;
    }

    /**
     * Updates a service.
     *
     * @param $id
     * @param array $data
     * @return bool
     * @throws DuplicateServiceException
     * @throws FailedServiceUpdateException
     * @throws ServiceNotFoundException
     */
    public function updateService($id, array $data)
    {
        $service = $this->findService($id);

        if (! $this->isServiceSlugUnique($data['slug'], true)) {
            throw new DuplicateServiceException;
        }

        try {
            DB::transaction(function () use ($service, $data) {
                $service->update([
                    'name' => $data['name'],
                    'slug' => $data['slug'],
                    'url' => $data['url'],
                ]);
            });

            return true;
        } catch (ModelNotFoundException $e) {
            throw new ServiceNotFoundException;
        } catch (\Exception $e) {
            throw new FailedServiceUpdateException;
        }
    }

    /**
     * Deletes service by ID.
     *
     * @param $id
     * @return bool
     */
    public function deleteService($id)
    {
        return $this->repo->delete($id);
    }

    /**
     * Get routes.
     *
     * @param null $service_id
     * @return Route[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getRoutes($service_id = null)
    {
        if (is_null($service_id)) {
            return Route::with('service')->get();
        }

        $this->findService($service_id)->routes;
    }

    /**
     * Adds route to a service.
     *
     * @param $id
     * @param array $data
     * @return mixed
     * @throws DuplicateRouteException
     * @throws FailedRouteCreationException
     * @throws ServiceNotFoundException
     */
    public function addRoute($id, array $data)
    {
        $service = $this->findService($id);

        if (! $this->isRouteSlugUnique($data['slug'], $service->slug)) {
            throw new DuplicateRouteException;
        }

        try {
            $route = null;

            DB::transaction(function () use ($service, $data, &$route) {
                $method = strtoupper($data['method']);

                if (! in_array($method, $this->allowed_methods)) {
                    throw new InvalidRouteMethodException;
                }

                $route = $service->routes()->create([
                    'slug' => $data['slug'],
                    'path' => $data['path'],
                    'target' => $data['target'],
                    'method' => $method,
                    'description' => ! isset($data['description']) ? '' : $data['description'],
                    'aggregate' => ! isset($data['aggregate']) ? false : $data['aggregate'],
                    'protected' => ! isset($data['protected']) ? false : $data['protected'],
                ]);

                $route->namespace = $data['namespace'];
                $route->path = $data['path'];
                $route->target = $data['target'];
                $route->save();

                if (isset($data['routes']) && ! empty($data['routes'])) {
                    collect($data['routes'])->forEach(function ($sub_route) use ($route) {
                        $route->children()->create([
                            'path' => $sub_route['path'],
                            'method' => strtoupper($sub_route['method']),
                            'description' => $sub_route['description'],
                            'aggregate' => false,
                        ]);
                    });
                }
            });

            event(new RoutesAltered);

            return $route;
        } catch (\Exception $e) {
            throw $e;
            throw new FailedRouteCreationException;
        }
    }

    /**
     * Finds route by ID.
     *
     * @param $id
     * @return Route
     */
    public function findRoute($id)
    {
        return Route::findOrFail($id);
    }

    /**
     * Finds route by slug.
     *
     * @param $slug
     * @return Route
     * @throws RouteNotFoundException
     */
    public function findRouteBySlug($slug)
    {
        try {
            return Route::where('slug', $slug)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new RouteNotFoundException;
        }
    }

    /**
     * Updates a service's route.
     *
     * @param $id
     * @param $route_id
     * @param array $data
     * @return bool
     * @throws ServiceNotFoundException
     * @throws DuplicateRouteException
     * @throws RouteNotFoundException
     * @throws FailedRouteUpdateException
     */
    public function updateRoute($id, $route_id, array $data)
    {
        $service = $this->findService($id);
        $route = $this->findRoute($route_id);

        if (! $this->isRouteSlugUnique($data['slug'], $service->slug, true)) {
            throw new DuplicateRouteException;
        }

        try {
            DB::transaction(function () use ($route, $data) {
                $method = strtoupper($data['method']);

                if (! in_array($method, $this->allowed_methods)) {
                    throw new InvalidRouteMethodException;
                }

                $route->update([
                    'slug' => $data['slug'],
                    'path' => $data['path'],
                    'method' => $method,
                    'description' => ! isset($data['description']) ? '' : $data['description'],
                    'aggregate' => ! isset($data['aggregate']) ? false : $data['aggregate'],
                    'protected' => ! isset($data['protected']) ? false : true,
                ]);

                $route->namespace = $data['namespace'];
                $route->path = $data['path'];
                $route->target = $data['target'];
                $route->save();

                if (isset($data['routes']) && ! empty($data['routes'])) {
                    $route->children->each->delete();

                    collect($data['routes'])->forEach(function ($sub_route) use ($route) {
                        $route->children()->create([
                            'path' => $sub_route['path'],
                            'method' => strtoupper($sub_route['method']),
                            'description' => $sub_route['description'],
                            'aggregate' => false,
                        ]);
                    });
                }
            });

            event(new RoutesAltered);

            return true;
        } catch (ModelNotFoundException $e) {
            throw new RouteNotFoundException;
        } catch (\Exception $e) {
            throw new FailedRouteUpdateException;
        }
    }

    /**
     * Removes route from a service.
     *
     * @param $id
     * @param $route_id
     * @return bool
     * @throws FailedRouteRemovalException
     */
    public function removeRoute($id, $route_id)
    {
        $service = $this->findService($id);
        $route = $this->findRoute($route_id);

        try {
            DB::transaction(function () use ($route) {
                $route->delete();
            });

            event(new RoutesAltered);

            return true;
        } catch (\Exception $e) {
            throw new FailedRouteRemovalException;
        }
    }

    /**
     * Returns normalized routes for caching.
     *
     * @return array
     */
    protected function getNormalizedRoutes()
    {
        $routes = $this->getRoutes()
            ->reduce(function ($carry, $route) {
                $alias = $route->service->slug . '.' . $route->slug;

                $carry[$alias] = [
                    'method' => $route->method,
                    'path' => $route->full_path,
                    'as' => $alias,
                    'url' => $route->target_url,
                    'protected' => $route->protected,
                ];

                return $carry;
            }, []);

        return $routes;
    }

    /**
     * Cache the routes into JSON file.
     *
     * @return bool
     * @throws RouteCachingDisabledException
     */
    public function cacheRoutes()
    {
        if (! env('CACHE_ROUTES')) {
            return true;
        }

        if (! Storage::exists('routes/cache.json')) {
            Storage::put('routes/cache.json', '[]');
        }

        $routes_json = json_encode($this->getNormalizedRoutes());

        Storage::put('routes/cache.json', $routes_json);

        return true;
    }

    /**
     * Returns the cache from JSON file.
     *
     * @return mixed
     */
    protected function getRoutesCache()
    {
        if (! Storage::exists('routes/cache.json')) {
            $this->cacheRoutes();
        }

        $routes_json = Storage::get('routes/cache.json');

        return json_decode($routes_json, true);
    }

    /**
     * Register all routes to app.
     *
     * @param Application $app
     * @return bool
     */
    public function registerRoutes(Application $app)
    {
        if (Schema::hasTable('routes')) {
            collect($this->getFinalRoutes())->each(function ($route, $key) use ($app) {
                $path = $route['path'];
                $method = strtolower($route['method']);

                $config = [
                    'uses' => '\App\Http\Controllers\GatewayController@' . $method,
                    'as' => $key,
                ];

                if (isset($route['protected']) && $route['protected']) {
                    $config['middleware'] = ['auth'];
                }

                $app->router->{$method}($path, $config);
            });

            return true;
        }

        return false;
    }

    /**
     * Returns routes based on environment.
     *
     * @return array
     */
    public function getFinalRoutes()
    {
        if (env('CACHE_ROUTES')) {
            return $this->getRoutesCache();
        }

        return $this->getNormalizedRoutes();
    }

    /**
     * Returns route data by alias.
     *
     * @param $alias
     * @return mixed
     */
    public function getRouteData($alias)
    {
        return $this->getFinalRoutes()[$alias];
    }
}
