<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class HttpService
{
    /**
     * @var Client
     */
    protected $guzzle;

    /**
     * HttpService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->guzzle = $client;
    }

    /**
     * Performs GET request.
     *
     * @param $url
     * @param $data
     * @param array $headers
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($url, $data, $headers = [])
    {
        try {
            $response = $this->guzzle->request('GET', $url, [
                'query' => $data,
                'headers' => $headers,
            ]);

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (RequestException $e) {
            $response = $e->getResponse();

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Performs POST request.
     *
     * @param $url
     * @param $data
     * @param array $headers
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($url, $data, $headers = [])
    {
        try {
            $response = $this->guzzle->request('POST', $url, [
                'json' => $data,
                'headers' => $headers,
            ]);

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (RequestException $e) {
            $response = $e->getResponse();

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * POST request with multipart
     *
     * @param $url
     * @param $data
     * @param array $headers
     * @return void
     */
    public function postMultipart($url, $data, $headers = [])
    {
        try {
            $response = $this->guzzle->request('POST', $url, [
                'headers' => $headers,
                'multipart' => $data,
            ]);

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (RequestException $e) {
            $response = $e->getResponse();

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Performs PATCH request.
     *
     * @param $url
     * @param $data
     * @param array $headers
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function patch($url, $data, $headers = [])
    {
        try {
            $response = $this->guzzle->request('PATCH', $url, [
                'json' => $data,
                'headers' => $headers,
            ]);

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (RequestException $e) {
            $response = $e->getResponse();

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Performs DELETE request.
     *
     * @param $url
     * @param array $headers
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete($url, $headers = [])
    {
        try {
            $response = $this->guzzle->request('DELETE', $url, [
                'headers' => $headers,
            ]);

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (RequestException $e) {
            $response = $e->getResponse();

            return [
                'status' => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody()),
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
