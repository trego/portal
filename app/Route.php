<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = [
        'id',
        'parent_route_id',
        'service_id',
        'slug',
        'path',
        'method',
        'description',
        'aggregate',
        'namespace',
        'target',
        'protected',
    ];

    protected $casts = [
        'aggregate' => 'boolean',
        'protected' => 'boolean',
    ];

    protected $hidden = [
        'parent_route_id',
        'service_id',
    ];

    /**
     * Setter for path.
     *
     * @param $value
     */
    public function setPathAttribute($value)
    {
        if (substr($value, 0, 1) === '/') {
            $this->attributes['path'] = substr($value, 1, strlen($value) - 1);
        } else {
            $this->attributes['path'] = $value;
        }
    }

    /**
     * Setter for namespace.
     *
     * @param $value
     */
    public function setNamespaceAttribute($value)
    {
        if (substr($value, 0, 1) === '/') {
            $this->attributes['namespace'] = substr($value, 1, strlen($value) - 1);
        } else {
            $this->attributes['namespace'] = $value;
        }
    }

    /**
     * Setter for target.
     *
     * @param $value
     */
    public function setTargetAttribute($value)
    {
        if (substr($value, 0, 1) === '/') {
            $this->attributes['target'] = substr($value, 1, strlen($value) - 1);
        } else {
            $this->attributes['target'] = $value;
        }
    }

    /**
     * Getter for public_url.
     *
     * @return string
     */
    public function getPublicUrlAttribute() {
        return env('APP_URL') . '/' . $this->namespace . '/' . $this->path;
    }

    /**
     * Getter for target_url.
     *
     * @return string
     */
    public function getTargetUrlAttribute() {
        return $this->service->url . '/' . $this->target;
    }

    /**
     * Getter for full_path.
     *
     * @return string
     */
    public function getFullPathAttribute() {
        return $this->namespace . '/' . $this->path;
    }

    /**
     * Returns this route's service.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * Returns this route's children.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Route::class, 'parent_route_id');
    }
}
