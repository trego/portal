<?php

namespace App\Exceptions;

class FailedRouteCreationException extends \Exception
{
    protected $message = 'Failed to create route';
}
