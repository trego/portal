<?php

namespace App\Exceptions;

class RouteCachingDisabledException extends \Exception
{
    protected $message = 'Route caching is disabled.';
}