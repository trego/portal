<?php

namespace App\Exceptions;

class FailedRouteUpdateException extends \Exception
{
    protected $message = 'Failed to update route';
}