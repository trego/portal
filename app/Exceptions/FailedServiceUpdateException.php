<?php

namespace App\Exceptions;

class FailedServiceUpdateException extends \Exception
{
    protected $message = 'Failed to update service';
}