<?php

namespace App\Exceptions;

class DuplicateRouteException extends \Exception
{
    protected $message = 'Duplicate route detected';
}