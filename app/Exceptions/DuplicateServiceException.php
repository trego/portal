<?php

namespace App\Exceptions;

class DuplicateServiceException extends \Exception
{
    protected $message = 'Duplicate service detected';
}