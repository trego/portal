<?php

namespace App\Exceptions;

class InvalidRouteMethodException extends \Exception
{
    protected $message = 'Only GET, POST, PUT, PATCH, and DELETE methods allowed';
}