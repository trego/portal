<?php

namespace App\Exceptions;

class ServiceNotFoundException extends \Exception
{
    protected $message = 'Service not found';
}