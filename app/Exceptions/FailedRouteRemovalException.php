<?php

namespace App\Exceptions;

class FailedRouteRemovalException extends \Exception
{
    protected $message = 'Failed to remove route';
}