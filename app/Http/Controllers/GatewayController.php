<?php

namespace App\Http\Controllers;

use App\Services\HttpService;
use App\Services\RoutingService;
use Illuminate\Http\Request;

class GatewayController extends Controller
{
    /**
     * Stores the route.
     *
     * @var \Illuminate\Routing\Route|object|string
     */
    protected $route;

    /**
     * Route alias.
     *
     * @var string
     */
    protected $route_name;

    /**
     * @var array
     */
    protected $route_params;

    /**
     * Route details.
     *
     * @var array
     */
    protected $route_data;

    /**
     * Request headers.
     *
     * @var array
     */
    protected $headers;

    public function __construct(Request $request, RoutingService $service)
    {
        $this->route = $request->route();
        $this->route_name = $this->route[1]['as'];
        $this->route_data = $service->getRouteData($this->route_name);
        $this->setHeaders($request);

        if ($this->isParameterizedRequest()) {
            $this->route_params = $this->route[2];
        }
    }

    /**
     * Sets proper headers.
     *
     * @param Request $request
     */
    protected function setHeaders(Request $request)
    {
        $this->headers = [
            'User-Agent' => $request->header('User-Agent'),
            'Authorization' => $request->header('Authorization'),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
    }

    /**
     * Checks if route is parameterized.
     *
     * @return bool
     */
    protected function isParameterizedRequest()
    {
        return ! empty($this->route[2]);
    }

    /**
     * Replace wildcard on parameterized request.
     *
     * @param $initial_url
     * @return string
     */
    protected function translateUrl($initial_url)
    {
        if (! $this->isParameterizedRequest()) {
            return $initial_url;
        }

        collect($this->route_params)
            ->each(function ($value, $key) use (&$initial_url) {
                $wildcard = '{' . $key . '}';

                if (strpos($initial_url, $wildcard) !== false) {
                    $initial_url = str_replace($wildcard, $value, $initial_url);
                }
            });

        return $initial_url;
    }

    /**
     * Returns final URL for the request.
     *
     * @return string
     */
    public function getUrl()
    {
        if ($this->isParameterizedRequest()) {
            return $this->translateUrl($this->route_data['url']);
        }

        return $this->route_data['url'];
    }

    /**
     * Handles GET request.
     *
     * @param Request $request
     * @param HttpService $http
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(Request $request, HttpService $http)
    {
        try {
            $response = $http->get(
                $this->getUrl(),
                $request->toArray(),
                $this->headers
            );

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }

    /**
     * Handles POST request.
     *
     * @param Request $request
     * @param HttpService $http
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(Request $request, HttpService $http)
    {
        try {
            if (count($request->file()) === 0) {
                $response = $http->post(
                    $this->getUrl(),
                    $request->toArray(),
                    $this->headers
                );
            } else {
                $data = collect($request->file())
                    ->map(function ($item, $key) {
                        return [
                            'name' => $key,
                            'contents' => fopen($item->getRealPath(), 'r'),
                            'filename' => $item->getClientOriginalName(),
                        ];
                    })
                    ->all();

                $headers = $this->headers;
                unset($headers['Content-Type']);

                $response = $http->postMultipart(
                    $this->getUrl(),
                    $data,
                    $headers
                );
            }

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }

    /**
     * Handles PATCH request.
     *
     * @param Request $request
     * @param HttpService $http
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function patch(Request $request, HttpService $http)
    {
        try {
            $response = $http->patch(
                $this->getUrl(),
                $request->toArray(),
                $this->headers
            );

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }

    /**
     * Handles DELETE request.
     *
     * @param Request $request
     * @param HttpService $http
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(Request $request, HttpService $http)
    {
        try {
            $response = $http->delete(
                $this->getUrl(),
                $this->headers
            );

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }
}
