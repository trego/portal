<?php

namespace App\Listeners;

use App\Events\RoutesAltered;
use App\Services\RoutingService;
use Illuminate\Support\Facades\Log;

class RebuildRoutesCache
{
    /**
     * Handles the event.
     *
     * @param RoutesAltered $event
     */
    public function handle(RoutesAltered $event)
    {
        if (! env('CACHE_ROUTES')) {
            Log::info('Route cahing disabled. Skipped routes cache rebuild');

            return;
        }

        try {
            $service = app(RoutingService::class);
            $service->cacheRoutes();

            Log::info('Route caches rebuilt at ' . date('Y-m-d H:i:s'));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}