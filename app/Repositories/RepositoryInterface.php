<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface RepositoryInterface
{
    /**
     * Returns all models.
     *
     * @return Collection
     */
    public function get(): Collection;

    /**
     * Returns model by ID.
     *
     * @param $id
     * @return Model
     * @throws ModelNotFoundException
     */
    public function find($id): Model;

    /**
     * Deletes a model by ID.
     *
     * @param $id
     * @return bool
     */
    public function delete($id): bool;
}
