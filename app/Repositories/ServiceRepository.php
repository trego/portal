<?php

namespace App\Repositories;

use App\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ServiceRepository implements RepositoryInterface
{
    public function get(): Collection
    {
        return Service::with('routes')->get();
    }

    public function find($id): Model
    {
        return Service::with('routes')->findOrFail($id);
    }

    public function delete($id): bool
    {
        $service = $this->find($id);
        $service->routes->each->delete();

        return $service->delete();
    }
}