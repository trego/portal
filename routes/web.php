<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** Allow OPTIONS request */
$router->options('{all:.*}', function () {
    return response()->json(null, 200);
});

$router->get('/', function () use ($router) {
    return [
        'name' => env('APP_DISPLAY_NAME'),
        'description' => env('APP_DESCRIPTION'),
    ];
});
