<?php

use App\Route;
use App\Services\RoutingService;
use Laravel\Lumen\Testing\DatabaseMigrations;

class RoutingServiceTest extends TestCase
{
    use DatabaseMigrations;

    public function testGetServices()
    {
        $service = new RoutingService;

        $this->assertTrue($service->getServices() instanceof \Illuminate\Support\Collection);
    }

    public function testAddService()
    {
        $service = new RoutingService;
        $new_service = $service->addService([
            'slug' => 'users',
            'name' => 'User Service',
            'url' => 'http://localhost:8000'
        ]);

        $this->assertTrue($new_service instanceof \App\Service);
    }

    public function testFindService()
    {
        $service = new RoutingService;
        $new_service = $service->addService([
            'slug' => 'users',
            'name' => 'User Service',
            'url' => 'http://localhost:8000'
        ]);

        $found_service = $service->findService($new_service->id);

        $this->assertTrue($found_service->id === $new_service->id);
    }

    public function testUpdateService()
    {
        $service = new RoutingService;
        $new_service = $service->addService([
            'slug' => 'users',
            'name' => 'User Service',
            'url' => 'http://localhost:8000'
        ]);

        $update_success = $service->updateService($new_service->id, [
            'slug' => 'users',
            'name' => 'UserService',
            'url' => 'http://localhost:8001',
        ]);

        $this->assertTrue($update_success);
    }

    public function testDeleteService()
    {
        $service = new RoutingService;
        $new_service = $service->addService([
            'slug' => 'users',
            'name' => 'User Service',
            'url' => 'http://localhost:8000'
        ]);

        $delete_success = $service->deleteService($new_service->id);

        $this->assertTrue($delete_success);
    }

    public function testRouteSetters()
    {
        $route = new Route;
        $route->target = '/users/{id}';
        $route->path = '/users/{id}';
        $route->method = 'GET';
        $route->slug = 'users.show';
        $route->save();

        $this->assertTrue($route->path === 'users/{id}');
        $this->assertTrue($route->target === 'users/{id}');
    }

    public function testAddRoute()
    {
        $service = new RoutingService;
        $new_service = $service->addService([
            'slug' => 'users',
            'name' => 'User Service',
            'url' => 'http://localhost:8000'
        ]);

        $new_route = $service->addRoute($new_service->id, [
            'slug' => 'index',
            'path' => 'users',
            'method' => 'GET',
            'description' => 'Get all users',
            'namespace' => 'v1',
            'target' => 'users',
            'protected' => true,
        ]);

        $this->assertInstanceOf(\App\Route::class, $new_route);
    }

    public function testUpdateRoute()
    {
        $service = new RoutingService;
        $new_service = $service->addService([
            'slug' => 'users',
            'name' => 'User Service',
            'url' => 'http://localhost:8000',
        ]);

        $new_route = $service->addRoute($new_service->id, [
            'slug' => 'index',
            'path' => 'users',
            'method' => 'GET',
            'description' => 'Get all users',
            'namespace' => 'v1',
            'target' => 'create',
        ]);

        $update_success = $service->updateRoute($new_service->id, $new_route->id, [
            'slug' => 'idx',
            'path' => '/idx',
            'method' => 'POST',
            'description' => 'Boom',
            'namespace' => 'v2',
            'target' => 'users',
        ]);

        $this->assertTrue($update_success);
    }

    public function testRemoveRoute()
    {
        $service = new RoutingService;
        $new_service = $service->addService([
            'slug' => 'users',
            'name' => 'User Service',
            'url' => 'http://localhost:8000'
        ]);

        $new_route = $service->addRoute($new_service->id, [
            'slug' => 'index',
            'path' => 'users',
            'method' => 'GET',
            'description' => 'Get all users',
            'namespace' => 'v1',
            'target' => 'users'
        ]);

        $remove_success = $service->removeRoute($new_service->id, $new_route->id);

        $this->assertTrue($remove_success);
    }

    public function testCacheRoutes()
    {
        $service = new RoutingService;

        $this->assertTrue($service->cacheRoutes());
    }

    public function testGetFinalRoutes()
    {
        $service = new RoutingService;
        $string = $service->getFinalRoutes();

        $this->assertTrue(is_array($string));
    }

    public function testRegisterRoutes()
    {
        $service = new RoutingService;

        $this->assertTrue($service->registerRoutes(app()));
    }
}
