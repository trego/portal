FROM composer AS build

WORKDIR /build

ADD . .
RUN composer install

FROM webdevops/php-nginx:alpine-php7

WORKDIR /app

COPY --from=build /build .

RUN chown -R 1000:1000 /app/storage
RUN chmod -R 755 /app/storage

ENV WEB_DOCUMENT_ROOT /app/public
